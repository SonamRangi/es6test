Install the "vscode". Open the "vscode"
Copy the link "https://github.com/rhildred/es6test.git" and Run the link.

<a href="https://github.com/rhildred/es6test" target="_blank">es6test</a>
A simple es6 starting point with vscode debugging and synchronous console input.

## License

es6test is released under the MIT license